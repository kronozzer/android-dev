package com.example.admin.officecrime;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Date;
import java.util.UUID;

public class CrimeFragment extends Fragment {

    public static final String EXTRA_CRIME_ID = "com.example.admin.officecrime.crime_id";
    private static final String TAG = "CrimeFragment";
    private static final String DIALOG_DATE = "date";
    private static final String DIALOG_IMAGE = "image";
    //Константа, по которой определим какой фрагмент возвращает значение
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_PHOTO = 1;
    private static final int REQUEST_CONTACT = 2;

    private Crime mCrime;
    private EditText mCrimeTitle;
    private Button mDateButton;
    private CheckBox mSolvedCheckBox;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;
    private Button mSuspectButton;
    private Callbacks mCallbacks;

    /**
     * Обязательный интерфейс для активности-хоста
     */
    public interface Callbacks {
        void onCrimeUpdated (Crime crime);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //Извлекаем из интента ID кликнутого в листе преступления UPD извлекаем из Arguments
        //UUID crimeId = (UUID) getActivity().getIntent().getSerializableExtra(EXTRA_CRIME_ID);
        UUID crimeId = (UUID) getArguments().getSerializable(EXTRA_CRIME_ID);
        //Вытаскиваем в переменную нужный объект из CrimeLab - набора преступлений по его UUID
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime, parent, false);

        mPhotoButton = (ImageButton)v.findViewById(R.id.crime_imageButton);
        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Кнопка с камерой создает новый интент и запускает CrimeCameraActivity
                Intent i = new Intent(getActivity(), CrimeCameraActivity.class);
                //startActivity(i);
                //Переделываем startActivity в startActivityForResult
                startActivityForResult(i, REQUEST_PHOTO);
            }
        });

        // Если камера недоступна, заблокировать функциональность
        // работы с камерой
        PackageManager pm = getActivity().getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) &&
                !pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            mPhotoButton.setEnabled(false);
        }

        //Включение кнопки Up
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            //Проверка, нужно ли её рисовать, есть ли у нее родительская активность
            if (NavUtils.getParentActivityName(getActivity()) != null) {
                getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }

        mPhotoView = (ImageView) v.findViewById(R.id.crime_imageView);
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Photo p = mCrime.getPhoto();
                if (p == null)
                    return;
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                String path = getActivity()
                        .getFileStreamPath(p.getFilename()).getAbsolutePath();
                //Вызываем метод созданного нами класса (ImageFragment) и в качестве аргумента передаем путь.
                ImageFragment.newInstance(path).show(fm, DIALOG_IMAGE);
            }
        });

        mCrimeTitle = (EditText) v.findViewById(R.id.crimeTitle);
        mCrimeTitle.setText(mCrime.getTitle());

        mCrimeTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCrime.setTitle(s.toString());
                //Обновляем UI (список слева)
                mCallbacks.onCrimeUpdated(mCrime);
                //Выставляем новый заголовок
                getActivity().setTitle(mCrime.getTitle());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDateButton = (Button) v.findViewById(R.id.crime_date);

        /*mDateButton.setText(mCrime.getDate().toString());*/
        updateDate();
        /*mDateButton.setEnabled(false);*/
        mDateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                /*DatePickerFragment dialog = new DatePickerFragment();*/
                //Вместо дефолтного ДэйтПикера передаем аргументы преступления (даты), которая появится по умолчанию.
                DatePickerFragment dialog = DatePickerFragment.newInstance(mCrime.getDate());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });

        mSolvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setChecked(mCrime.isSolved());
        mSolvedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                // Устанавливаем флажок 0 или 1
                mCrime.setSolved(isChecked);
                //Обновление UI новыми данными, при изменении Преступления
                mCallbacks.onCrimeUpdated(mCrime);
            }
        });

        Button reportButton = (Button) v.findViewById(R.id.crime_reportButton);
        reportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getCrimeReport());
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.crime_report_subject));
                i = Intent.createChooser(i, getString(R.string.send_report));
                startActivity(i);
            }
        });

        mSuspectButton = (Button) v.findViewById(R.id.crime_suspectButton);
        mSuspectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Неявный интент на пик контактов из книги
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
            }
        });
        if (mCrime.getSuspect() != null) {
            mSuspectButton.setText(mCrime.getSuspect());
        }

        return v;
    }

    public static CrimeFragment newInstance (UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CRIME_ID, crimeId);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        //Чтоб не хардкодить - нужно потом заменить на switch
        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mCrime.setDate(date);
            //Обновление Даты UI
            mCallbacks.onCrimeUpdated(mCrime);
            /*mDateButton.setText(mCrime.getDate().toString());*/
            updateDate();
        } else  if (requestCode == REQUEST_PHOTO) {
                // Создание нового объекта Photo и связывание его с Crime
                String filename = data.getStringExtra(CrimeCameraFragment.EXTRA_PHOTO_FILENAME);
                if (filename != null) {
                    /*Log.i(TAG, "filename: " + filename);*/
                    //Создаем фото по его пути
                    Photo p = new Photo(filename);
                    //И обновляем модель данных Crime
                    mCrime.setPhoto(p);
                    /*Log.i(TAG, "Crime: " + mCrime.getTitle() + " has a photo");*/
                    //Обновление модели при смене фото
                    mCallbacks.onCrimeUpdated(mCrime);
                    showPhoto();
                } else if (requestCode == REQUEST_CONTACT) {
                    Uri contactUri = data.getData();
                    // Определение полей, значения которых должны быть
                    // возвращены запросом.
                    String[] queryFields = new String[] { ContactsContract.Contacts.DISPLAY_NAME };
                    // Выполнение запроса - contactUri здесь выполняет функции
                    // условия "where"
                    Cursor c = getActivity().getContentResolver().query(contactUri, queryFields, null, null, null);
                    // Проверка получения результатов
                    if (c.getCount() == 0) {
                        c.close();
                        return;
                    }
                    // Извлечение первого столбца данных - имени подозреваемого.
                    c.moveToFirst();
                    String suspect = c.getString(0);
                    mCrime.setSuspect(suspect);
                    //Обновление модели Crime для подозреваемых
                    mCallbacks.onCrimeUpdated(mCrime);
                    mSuspectButton.setText(suspect);
                    c.close();
                }
        }
    }

    public void updateDate() {
        mDateButton.setText(mCrime.getDate().toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Проверяем указана ли в манифесте родительская активность, да - переходим к ней.
                if (NavUtils.getParentActivityName(getActivity()) != null) {
                    NavUtils.navigateUpFromSameTask(getActivity());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Сохранение данных
        CrimeLab.get(getActivity()).saveCrimes();
    }

    @Override
    public void onStart() {
        super.onStart();
        //Отрисовываем фото
        showPhoto();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Очищаем память от всех представлений Bitmap
        PictureUtils.cleanImageView(mPhotoView);
    }

    private void showPhoto() {
        // Назначение изображения, полученного на основе фотографии
        Photo p = mCrime.getPhoto();
        BitmapDrawable b = null;
        if (p != null) {
            String path = getActivity().getFileStreamPath(p.getFilename()).getAbsolutePath();
            b = PictureUtils.getScaledDrawable(getActivity(), path);
        }
        mPhotoView.setImageDrawable(b);
    }

    private String getCrimeReport() {
        String solvedString = null;
        if (mCrime.isSolved()) {
            solvedString = getString(R.string.crime_report_solved);
        } else {
            solvedString = getString(R.string.crime_report_unsolved);
        }
        String dateFormat = "EEEE, dd MMMM";
        String dateString = DateFormat.format(dateFormat, mCrime.getDate()).toString();
        String suspect = mCrime.getSuspect();
        if (suspect == null) {
            suspect = getString(R.string.crime_report_no_suspect);
        } else {
            suspect = getString(R.string.crime_report_suspect, suspect);
        }
        String report = getString(R.string.crime_report,
                mCrime.getTitle(), dateString, solvedString, suspect);
        return report;
    }
}
