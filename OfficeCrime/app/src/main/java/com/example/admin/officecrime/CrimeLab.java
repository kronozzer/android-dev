package com.example.admin.officecrime;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

public class CrimeLab {

    private ArrayList<Crime> mCrimes;
    //Единственная статическая переменная синглетоновского класса, т.е. ЕДИНСТВЕННЫЙ ЭКЗЕМПЛЯР
    private static CrimeLab sCrimeLab;
    private Context mAppContext;
    private static final String TAG = "CrimeLab";
    private static final String FILENAME = "crimes.json";
    private CriminalIntentJSONSerializer mSerializer;

    //SINGLETON pattern: private constructor, public get()
    //Lazy
    private CrimeLab(Context appContext)
    {
        mAppContext = appContext;
        mSerializer = new CriminalIntentJSONSerializer(mAppContext, FILENAME);
        try {
            //Загружаем объекты
            mCrimes = mSerializer.loadCrimes();
        } catch (Exception e) {
            //Если возникло исключение в загрузке - создаем новый объект преступлений
            mCrimes = new ArrayList<Crime>();
            Log.e(TAG, "Error loading crimes: ", e);
        }
        //Автогенерация преступлений
        /*for (int i = 0; i < 100; i++) {
            Crime c = new Crime();
            c.setTitle("Преступление № " + (i+1));
            c.setSolved(i % 2 == 0);
            mCrimes.add(c);
        }*/
    }

    public static CrimeLab get(Context c) {
        if (sCrimeLab == null) {
            sCrimeLab = new CrimeLab(c.getApplicationContext());
        }
        return sCrimeLab;
    }

    public ArrayList<Crime> getCrimes() {
        return mCrimes;
    }

    public Crime getCrime(UUID id) {
        for (Crime c : mCrimes){
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }

    public void addCrime(Crime c) {
        mCrimes.add(c);
    }

    public boolean saveCrimes() {
        try {
            mSerializer.saveCrimes(mCrimes);
            Log.d(TAG, "crimes saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving crimes: ", e);
            return false;
        }
    }

    public void deleteCrime(Crime c) {
        mCrimes.remove(c);
    }
}

/*
Существуют несколько различных подходов, реализации шаблона Singleton, но все они имеют общие принципы.
        Private конструктор — для запрета инициализации экземпляра класса из другого класса через конструктор.
        Private static переменную того же класса, которая и будет единственным экземпляром этого класса.
        Public static метод, возвращающий экземпляр класса. Это — глобальная точка доступа для внешнего мира позволяющая получить экземпляр класса Singleton.*/
