package com.example.admin.officecrime;

import org.json.JSONException;
import org.json.JSONObject;

public class Photo {
    private static final String JSON_FILENAME = "filename";
    private String mFilename;

    /** Создание объекта Photo, представляющего файл на диске */
    public Photo(String filename) {
        mFilename = filename;
    }

    /** метод сериализации JSON, который используется классом Crime для
    сохранения и загрузки свойства типа Photo */
    public Photo(JSONObject json) throws JSONException {
        mFilename = json.getString(JSON_FILENAME);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_FILENAME, mFilename);
        return json;
    }

    public String getFilename() {
        return mFilename;
    }
}
