package com.rocketjumpapps.saveinstancestate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class SaveInstance extends AppCompatActivity {

    private static final String VALUE = "VALUE";
    private static final String COL = "NUMBER";
    public int sum = 0;
    private String currentTextValue;
    private EditText editText;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            currentTextValue = null;
        } else {
            currentTextValue = savedInstanceState.getString(VALUE);
            sum = savedInstanceState.getInt(COL);
            sum++;
        }

        initGUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        currentTextValue = editText.getText().toString();
        outState.putString(VALUE, currentTextValue);
        sum = Integer.parseInt(textView.getText().toString());
        outState.putInt(COL, sum);
    }

    private void initGUI() {
        editText = (EditText) findViewById(R.id.edit_text);
        editText.setText(currentTextValue);
        textView = (TextView) findViewById(R.id.text_count);
        textView.setText(String.valueOf(sum));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save_instance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_exit:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
