package com.rocketjumpapps.googleplacessample;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.rocketjumpapps.googleplacessample.model.Place;
import com.rocketjumpapps.googleplacessample.model.Response;
import com.rocketjumpapps.googleplacessample.model.Result;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MapsInteractor implements IMapsInteractor {
    @Override
    public void getPlacesFromService(final LatLng latLng, final Context context, final onDataRetrieveListener listener) {
        System.out.print(latLng.latitude + "," + latLng.longitude);
        String url = String.format("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%s,%s&radius=1000&language=ru&key=%s",
                latLng.latitude, latLng.longitude, context.getResources().getString(R.string.google_maps_key_web));
        StringRequest getRequest = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.print(response);
                Response responseObj = new Gson().fromJson(response, Response.class);
                if (responseObj.getStatus().equals("OK")) {
                    List<Place> simpleRequestList = convertToSimplePlaceList(responseObj);
                    listener.onDataReceived(simpleRequestList);
                }
            }
        },
        new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        /*{
              @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                String strLatLng = latLng.latitude + "," + latLng.longitude;
                params.put("location", strLatLng);
                params.put("radius", "500");
                params.put("language", "ru");
                params.put("key", context.getResources().getString(R.string.google_maps_key));
                return params;
            }
        };
        */

        Volley.newRequestQueue(context).add(getRequest);
    }

    private List<Place> convertToSimplePlaceList(Response responseObj) {
        List<Place> places = new ArrayList<Place>();
        Place place = new Place();
        for (Result res : responseObj.getResults()) {
            place.setIcon(res.getIcon());
            place.setName(res.getName());
            try {
                place.setPhoto(res.getPhotos().get(0).getPhotoReference());
            } catch (Exception e) {
                place.setPhoto(null);
            }
            place.setVicinity(res.getVicinity());
            places.add(place);
        }
        return places;
    }
}
