package com.rocketjumpapps.googleplacessample;

import android.content.Context;

import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.rocketjumpapps.googleplacessample.MapsContract.MapView;
import com.rocketjumpapps.googleplacessample.model.Place;

import java.util.List;

public class MapsPresenter implements MapsContract.Presenter, MapsInteractor.onDataRetrieveListener {

    private MapView view;
    private MapsInteractor interactor;

    public MapsPresenter() {
        interactor = new MapsInteractor();
    }

    @Override
    public void onAttacheView(MapView view) {
        this.view = view;
    }

    @Override
    public void onDetachView() {
        view = null;
    }

    @Override
    public void onLocationChanged(LatLng coordinates) {
        if (view != null) {
            view.drawNewMarker(coordinates);
            view.showProgress();
            interactor.getPlacesFromService(coordinates, (Context) view, this);
        }
    }

    @Override
    public void onDataReceived(List<Place> places) {
        if (view != null) {
            view.hideProgress();
            view.clearPlacesList();
            view.refreshPlacesList(places);
        }
    }
}
