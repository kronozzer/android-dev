package com.rocketjumpapps.googleplacessample;

import com.google.android.gms.maps.model.LatLng;
import com.rocketjumpapps.googleplacessample.model.Place;

import java.util.List;

public class MapsContract {

    public interface MapView {

        void refreshPlacesList(List<Place> places);

        void clearPlacesList();

        void showProgress();

        void hideProgress();

        void drawNewMarker(LatLng coordinates);

    }

    public interface Presenter {

        void onAttacheView(MapView view);

        void onDetachView();

        void onLocationChanged(LatLng coordinates);

    }
}
