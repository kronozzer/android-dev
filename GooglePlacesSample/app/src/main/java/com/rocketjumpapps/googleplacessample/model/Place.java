package com.rocketjumpapps.googleplacessample.model;

public class Place {
    private String icon;
    private String name;
    private String photo;
    private String vicinity;

    public Place() {

    }

    public Place(String icon, String name, String photo, String vicinity) {
        this.icon = icon;
        this.name = name;
        this.photo = photo;
        this.vicinity = vicinity;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }
}
