package com.rocketjumpapps.googleplacessample;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.rocketjumpapps.googleplacessample.model.Place;

import org.json.JSONArray;

import java.util.List;

public interface IMapsInteractor {

    void getPlacesFromService(LatLng latLng, Context context, onDataRetrieveListener listener);

    interface onDataRetrieveListener {

        void onDataReceived(List<Place> places);

    }

}
