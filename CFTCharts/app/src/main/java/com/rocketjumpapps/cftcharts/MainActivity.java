package com.rocketjumpapps.cftcharts;

import android.content.Intent;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    private static final String LOG = "app_log";
    private EditText et_points;
    private Button letsGoButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        letsGoButton.setEnabled(true);
    }

    private void initGUI() {
        et_points = (EditText) findViewById(R.id.editText);
        letsGoButton = (Button) findViewById(R.id.letsGoButton);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        if (progressBar != null)
        progressBar.setVisibility(View.GONE);
        letsGoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendDataToServer().execute(et_points.getText().toString());
            }
        });
    }

    class SendDataToServer extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            letsGoButton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            JSONObject mainObject;
            JSONObject response;
            ArrayList<PointF> listOfPoints = new ArrayList<>();
            try {
                mainObject = new JSONObject(s);
                int result = mainObject.getInt("result");
                response = mainObject.getJSONObject("response");
                switch (result) {
                    case 0: {
                        JSONArray points = response.getJSONArray("points");
                        if (points != null) {
                            for (int i=0; i < points.length(); i++) {
                                JSONObject point = points.getJSONObject(i);
                                PointF currPoint = new PointF();
                                currPoint.set(Float.parseFloat(point.getString("x")), Float.parseFloat(point.getString("y")));
                                listOfPoints.add(currPoint);
                            }
                            Collections.sort(listOfPoints, new PointCompare());
                            Intent intent = new Intent(MainActivity.this, ChartActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putParcelableArrayListExtra("points", listOfPoints);
                            getApplicationContext().startActivity(intent);
                        }
                        break;
                    }
                    default: {
                        Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                        break;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpsURLConnection conn = null;
            String result = "";

            try {
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                            public void checkClientTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                            public void checkServerTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                };

                try {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                url = new URL("https://demo.bankplus.ru/mobws/json/pointsList");
                conn = (HttpsURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("charset", "utf-8");
                conn.setUseCaches(false);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("count", params[0])
                        .appendQueryParameter("version", "1.1");
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "utf-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();

                printCerts(conn);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                result = response.toString();
                Log.d(LOG, "Server request: " + result);

            } catch (IOException ex_url) {
                ex_url.printStackTrace();
            } finally {
                if(conn != null) {
                    conn.disconnect();
                }
            }
            return result;
        }

        private void printCerts(HttpsURLConnection conn) throws IOException {
            if (conn != null) {
                Log.d(LOG, "Response Code: " + conn.getResponseCode());
                Log.d(LOG, "Cipher Suite: " + conn.getCipherSuite());
                Log.d(LOG, "\n");

                Certificate[] certs = conn.getServerCertificates();
                for (Certificate cert : certs) {
                    Log.d(LOG, "Cert Type: " + cert.getType());
                    Log.d(LOG, "Cert Hash Code: " + cert.hashCode());
                    Log.d(LOG, "Cert Public Key Algorithm: "
                            + cert.getPublicKey().getAlgorithm());
                    Log.d(LOG, "Cert Public Key Format: "
                            + cert.getPublicKey().getFormat());
                    Log.d(LOG, "\n");
                }
            }
        }
    }

    public class PointCompare implements Comparator<PointF> {
        public int compare(PointF a, PointF b) {
            if (a.x < b.x) {
                return -1;
            }
            else if (a.x > b.x) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
}

