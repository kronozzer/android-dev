package com.rocketjumpapps.cftcharts;

import android.graphics.PointF;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.robinhood.spark.SparkAdapter;
import com.robinhood.spark.SparkView;

import java.util.ArrayList;

public class ChartActivity extends AppCompatActivity {

    private SparkView sparkView;
    private TextView scrubInfoTextView;
    private TableLayout tableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        Bundle extras = getIntent().getExtras();
        ArrayList<PointF> points = extras.getParcelableArrayList("points");
        if (points != null) {
            initGUI(points);
            addRow("Num", "Point(x)", "Point(y)");
            for (int i = 0; i < points.size(); i++) {
                addRow(Integer.toString(i + 1), Float.toString(points.get(i).x), Float.toString(points.get(i).y));
            }
        }
    }

    private void addRow(String num, String x, String y) {
        LayoutInflater inflater = LayoutInflater.from(this);
        TableRow tr = (TableRow) inflater.inflate(R.layout.grid_item, null);
        TextView tvNum = (TextView) tr.findViewById(R.id.num);
        TextView tvX = (TextView) tr.findViewById(R.id.x);
        TextView tvY = (TextView) tr.findViewById(R.id.y);
        tvNum.setText(num);
        tvX.setText(x);
        tvY.setText(y);
        if (num.equals("Num")) {
            tvNum.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
            tvX.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
            tvY.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        }
        tableLayout.addView(tr);
    }

    private void initGUI(ArrayList<PointF> points) {
        tableLayout = (TableLayout) findViewById(R.id.table);
        sparkView = (SparkView) findViewById(R.id.sparkview);
        sparkView.setAdapter(new MyAdapter(points));
        scrubInfoTextView = (TextView) findViewById(R.id.scrub_info_textview);
        sparkView.setScrubListener(new SparkView.OnScrubListener() {
            @Override
            public void onScrubbed(Object value) {
                if (value == null) {
                    scrubInfoTextView.setText(R.string.scrub_empty);
                } else {
                    scrubInfoTextView.setText(getString(R.string.scrub_format, value));
                }
            }
        });
    }

    public class MyAdapter extends SparkAdapter {
        private ArrayList<PointF> yData;

        public MyAdapter(ArrayList<PointF> yData) {
            this.yData = yData;
        }

        @Override
        public int getCount() {
            return yData.size();
        }

        @Override
        public Object getItem(int index) {
            return yData.get(index);
        }

        @Override
        public float getY(int index) {
            return yData.get(index).y;
        }

        @Override
        public float getX(int index) {
            return yData.get(index).x;
        }

        @Override
        public boolean hasBaseLine() { return true; }

        @Override
        public float getBaseLine() {
            return 0;
        }
    }
}
