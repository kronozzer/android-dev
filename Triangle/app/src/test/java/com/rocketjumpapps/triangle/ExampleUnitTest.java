package com.rocketjumpapps.triangle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleUnitTest {
    private Rtriangle triangle;
    private Point a, b, c;

    @Before
    public void setUp() throws Exception {
        triangle = RtriangleProvider.getRtriangle();
        //Перед тем как работать с объектом нужно проверить не пришел ли нам null от функции
        assertNotNull("Метод getRteiangle() вернул null", triangle);
        a = new Point(triangle.getApexX1(), triangle.getApexY1());
        b = new Point(triangle.getApexX2(), triangle.getApexY2());
        c = new Point(triangle.getApexX3(), triangle.getApexY3());
        //Так же нужно проверить треугольник ли это (точки не лежат на одной прямой или находятся друг на друге)
        assertNotEquals("Точки лежат на одной прямой либо совпадают", (a.x - c.x)*(b.y - c.y),(b.x - c.x)*(a.y - c.y));
    }

    @Test
    public void simplePifagorTest() throws Exception {
        //вычисление квадратов длин сторон треугольника
        int ab = a.getSqrLenght(b);
        int ac = a.getSqrLenght(c);
        int bc = b.getSqrLenght(c);
        //теорема Пифагора в квадратах (избавиться от корней)
        if (ab + ac == bc || ab + bc == ac || bc + ac == ab) {
            assertTrue("Треугольник прямоугольный", true);          //passed
        } else {
            assertFalse("Треугольник не прямоугольный", true);      //orNot
        }
    }

    @Test
    public void hardcoreFalesTest() throws Exception {
        /*TODO: Можно реализовать алгоритм и поинтереснее - на основе теоремы Фалеса, когда
        гипотенуза является диаметром описанной окружности прямоугольного треугольника. Ищем длинную сторону(гипотенузу),
        координату половины гипотенузы (центр окружности), строим окружность радиуса 1/2 гипотенузы и смотрим чтобы все
        3 вершины треугольника принадлежали окружности. Должно работать =)
        */
    }

    @After
    public void tearDown() {
        //Можно заняться сборкой мусора тут
        //System.gc();
    }
}