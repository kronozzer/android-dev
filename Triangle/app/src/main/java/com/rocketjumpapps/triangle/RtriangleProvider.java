package com.rocketjumpapps.triangle;

/**
 * Created by Alex on 27.03.2016.
 */
public final class RtriangleProvider {
    public static Rtriangle getRtriangle() {
        Rtriangle rightTriangle = new Rtriangle() {
            @Override
            public int getApexX1() {
                return 0;
            }

            @Override
            public int getApexY1() {
                return 0;
            }

            @Override
            public int getApexX2() { return 0; }

            @Override
            public int getApexY2() {
                return 4;
            }

            @Override
            public int getApexX3() {
                return 5;
            }

            @Override
            public int getApexY3() {
                return 0;
            }
        };
        return rightTriangle;
    }
}
