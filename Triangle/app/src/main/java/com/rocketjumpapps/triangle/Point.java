package com.rocketjumpapps.triangle;

/**
 * Created by Alex on 27.03.2016.
 */
public class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getSqrLenght(Point b) {
        int sqrLength = (int) ((Math.pow(b.x-this.x, 2))+(Math.pow(b.y-this.y, 2)));
        return sqrLength;
    }
}
