package com.rocketjumpapps.triangle;

/**
 * Created by Alex on 18.03.2016.
 */
public interface Rtriangle {
    int getApexX1();
    int getApexY1();
    int getApexX2();
    int getApexY2();
    int getApexX3();
    int getApexY3();
}
