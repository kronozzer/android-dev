package com.rocketjumpapps.improvetest;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SignUpActivity extends AppCompatActivity {

    private EditText fullName;
    private EditText dateOfBirth;
    private EditText email;
    private EditText userName;
    private EditText password;
    private FloatingActionButton send;
    private SimpleDateFormat sdf;
    private TextView title;
    private TextView counter;
    private Calendar myCalendar = Calendar.getInstance();
    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private boolean isValid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initGUI();
        sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        sdf.setLenient(false);
        animate();
    }

    private void animate() {
        Animation alpha = AnimationUtils.loadAnimation(this, R.anim.alpha);
        Animation leftTrans = AnimationUtils.loadAnimation(this, R.anim.ltrans);
        Animation rightTrans = AnimationUtils.loadAnimation(this, R.anim.rtrans);
        title.startAnimation(alpha);
        fullName.startAnimation(leftTrans);
        dateOfBirth.startAnimation(rightTrans);
        email.startAnimation(leftTrans);
        userName.startAnimation(rightTrans);
        password.startAnimation(leftTrans);
        send.startAnimation(rightTrans);
    }

    private void initGUI() {
        title = (TextView) findViewById(R.id.title);
        fullName = (EditText) findViewById(R.id.fullName);
        counter = (TextView) findViewById(R.id.counter);
        counter.setText(fullName.getText().length() + getString(R.string.counter));
        userName = (EditText) findViewById(R.id.userName);
        password = (EditText) findViewById(R.id.password);
        send = (FloatingActionButton) findViewById(R.id.fab);
        dateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SignUpActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dateOfBirth.addTextChangedListener(myDateWatcher);
        email = (EditText) findViewById(R.id.email);
        if (email != null) {
            email.addTextChangedListener(myEmailWatcher);
        }
        fullName.addTextChangedListener(myTextWatcher);
        userName.addTextChangedListener(myTextWatcher);
        password.addTextChangedListener(myTextWatcher);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateForm();
                if (isValid) {
                    Intent mailTo = new Intent(android.content.Intent.ACTION_SEND);
                    mailTo.setType("plain/text")
                            .putExtra(Intent.EXTRA_EMAIL, new String[]{email.getText().toString()})
                            .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject))
                            .putExtra(Intent.EXTRA_TEXT,
                                    "Full Name: " + fullName.getText().toString() + '\n' +
                                            "Date Of Birth: " + dateOfBirth.getText().toString() + '\n' +
                                            "E-mail: " + email.getText().toString() + '\n' +
                                            "User Name: " + userName.getText().toString() + '\n' +
                                            "Password: " + password.getText().toString());
                    startActivity(Intent.createChooser(mailTo, getString(R.string.email_intent_chooser)));
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.error_incorrect_sending_form), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateEditText();
        }

    };

    private TextWatcher myDateWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            isValid = isValidDate(working);

            if (!isValid) {
                dateOfBirth.setError(getString(R.string.error_invalid_birth));
            } else {
                dateOfBirth.setError(null);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        private boolean isValidDate(String date) {
            try {
                myCalendar.setTime(sdf.parse(date));
            } catch (ParseException e) {
                return false;
            }
            if (date.length()!=10) {
                return false;
            }
            if (Calendar.getInstance().before(myCalendar)) {
                return false;
            }
            return true;
        }

    };

    private TextWatcher myEmailWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            isValid = isValidEmail(email.getText().toString());
            if(!isValid){
                email.setError(getString(R.string.error_invalid_email));
            }
        }

        private boolean isValidEmail(String email) {
            return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    };

    private TextWatcher myTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (fullName.getText().hashCode() == s.hashCode()) {
                isValid = isValidText(fullName.getText().toString());
                counter.setText(fullName.getText().length() + getString(R.string.counter));
                if (!isValid) {
                    fullName.setError(getString(R.string.error_invalid_text));
                } else {
                    fullName.setError(null);
                }
            }
            if (userName.getText().hashCode() == s.hashCode()) {
                isValid = isValidText(userName.getText().toString());
                if (!isValid) {
                    userName.setError(getString(R.string.error_invalid_text));
                } else {
                    userName.setError(null);
                }
            }
            if (password.getText().hashCode() == s.hashCode()) {
                isValid = isValidText(password.getText().toString());
                if (!isValid) {
                    password.setError(getString(R.string.error_invalid_text));
                } else {
                    password.setError(null);
                }
            }
        }

        private boolean isValidText(String text) {
            return !text.equals("");
        }
    };

    private void updateEditText() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        dateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

    private void validateForm() {
        if (TextUtils.isEmpty(email.getText().toString())) {
            email.setError(getString(R.string.error_invalid_text));
            isValid = false;
        }
        if (TextUtils.isEmpty(userName.getText().toString())) {
            userName.setError(getString(R.string.error_invalid_text));
            isValid = false;
        }
        if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError(getString(R.string.error_invalid_text));
            isValid = false;
        }
        if (TextUtils.isEmpty(fullName.getText().toString())) {
            fullName.setError(getString(R.string.error_invalid_text));
            isValid = false;
        }
        if (TextUtils.isEmpty(dateOfBirth.getText().toString())) {
            dateOfBirth.setError(getString(R.string.error_invalid_text));
            isValid = false;
        }
    }
}
