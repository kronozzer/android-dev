package com.rocketjumpapps.flagquiz;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.renderscript.ScriptGroup;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class MainActivityFragment extends Fragment {

    private static final String TAG = "FlagQuizGame Fragment";

    private List<String> fileNameList;          // имена файлов флагов
    private List<String> quizCountriesList;     // имена стран в викторине
    private Map<String, Boolean> regionsMap;    // используемые регионы
    private String correctAnswer;               // корректная строка текущего флага
    //private String nextImageName;
    private int totalGuesses;                   // кол-во вопросов
    private int correctAnswers;                 // кол-во верных ответов
    private int guessRows;
    private Random random;
    private Handler handler;                    // задержка перед загрузкой следующего флага
    private Animation shakeAnimation;           // анимация неверного ответа
    private TextView answerTextView;
    private TextView questionNumberTextView;
    private ImageView flagImageView;
    private TableLayout buttonTableLayout;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        //Инициализирование GUI
        questionNumberTextView = (TextView) view.findViewById(R.id.questionNumberTextView);
        flagImageView = (ImageView) view.findViewById(R.id.flagImageView);
        buttonTableLayout = (TableLayout) view.findViewById(R.id.buttonTableLayout);
        answerTextView = (TextView) view.findViewById(R.id.answerTextView);

        //Настройка текста
        if (savedInstanceState == null) {
            questionNumberTextView.setText(getResources().getString(R.string.question) + " 1 " +
                    getResources().getString(R.string.of) + " 10");
            resetQuiz();
        }
        else {
            questionNumberTextView.setText(getResources().getString(R.string.question) + " " + (correctAnswers+1) + " " +
                    getResources().getString(R.string.of) + " 10");
            loadNextFlag(true);
        }

        return view;
    }

    public void resetQuiz() {
        // Доступ к изображению флага с помощью AssetManager
        AssetManager am = getActivity().getAssets();
        fileNameList.clear();                                       // очистка списка
        try {
            Set<String> regions = regionsMap.keySet();              // получение набора регионов
            for (String region : regions) {
                String[] paths = am.list(region);
                for (String path : paths) {
                    fileNameList.add(path.replace(".png", ""));
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Error Loading Images", e);
        }

        correctAnswers = 0;
        totalGuesses = 0;
        quizCountriesList.clear();

        int flagCounter = 1;
        int numberOfFlags = fileNameList.size();
        while (flagCounter <= 10) {
            int randomIndex = random.nextInt(numberOfFlags);
            String fileName = fileNameList.get(randomIndex);
            // если регион активен, но еще не выбран
            if (!quizCountriesList.contains(fileName)) {
                quizCountriesList.add(fileName);
                ++flagCounter;
            }
        }

        loadNextFlag(false);
    }

    private void loadNextFlag(boolean cur) {
        // получение имени флага для следующего и удаление его из списка
        if (!cur) {
            correctAnswer = quizCountriesList.remove(0);
        }

        answerTextView.setText("");
        questionNumberTextView.setText(
                getResources().getString(R.string.question) + " " +
                        (correctAnswers + 1) + " " +
                        getResources().getString(R.string.of) + " 10");

        String region = correctAnswer.substring(0, correctAnswer.indexOf('-'));
        AssetManager am = getActivity().getAssets();
        InputStream stream;

        try {
            stream = am.open(region + "/" + correctAnswer + ".png");
            // Загрузка ресурса в качестве Drawable
            Drawable flag = Drawable.createFromStream(stream, correctAnswer);
            flagImageView.setImageDrawable(flag);
        } catch (IOException e) {
            Log.e(TAG, "Error loading " + correctAnswer, e);
        }

        //Надо зачистить контролы от старой инфы
        for (int row = 0; row < buttonTableLayout.getChildCount(); ++row) {
            ((TableRow) buttonTableLayout.getChildAt(row)).removeAllViews();
        }

        Collections.shuffle(fileNameList);

        //помещение корректного ответа в конец списка fileNameList
        int correct = fileNameList.indexOf(correctAnswer);
        fileNameList.add(fileNameList.remove(correct));

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Добавление кнопок на основе значения guessRows
        for (int row = 0; row < guessRows; row++) {
            TableRow currentTableRow = getTableRow(row);
            //Помещение кнопок в TableRow
            for (int column = 0; column < 3; column++) {
                Button newGuessButton = (Button) inflater.inflate(R.layout.guess_button, null);
                String fileName = fileNameList.get((row * 3) + column);
                newGuessButton.setText(getCountryName(fileName));
                //добавляем лисенер на кнопку
                newGuessButton.setOnClickListener(guessButtonListener);
                currentTableRow.addView(newGuessButton);
            }
        }

        //Случайная замена кнопки корректным ответом.
        int row = random.nextInt(guessRows);                // Выбор случайной строки
        int column = random.nextInt(3);                     // Выбор случайного столбца
        TableRow randomTableRow = getTableRow(row);
        String countryName = getCountryName(correctAnswer);
        ((Button) randomTableRow.getChildAt(column)).setText(countryName);
    }

    private View.OnClickListener guessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            submitGuess((Button) v);
        }
    };

    private String getCountryName(String name) {
        return name.substring(name.indexOf('-') + 1).replace('_', ' ');
    }

    private TableRow getTableRow(int row) {
        return (TableRow) buttonTableLayout.getChildAt(row);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (savedInstanceState == null) {
            fileNameList = new ArrayList<String>();
            quizCountriesList = new ArrayList<String>();
            regionsMap = new HashMap<String, Boolean>();
            guessRows = 1;
            random = new Random();
            handler = new Handler();

            //загруска анимации
            shakeAnimation = AnimationUtils.loadAnimation(this.getContext(), R.anim.incorrect_shake);
            shakeAnimation.setRepeatCount(3);

            //массив регионов мира
            String[] regionNames = getResources().getStringArray(R.array.regionList);

            //по умолчанию - все регионы
            for (String region : regionNames) {
                regionsMap.put(region, true);
            }
        }
    }

    private void submitGuess (Button guessButton) {
        String guess = guessButton.getText().toString();
        String answer = getCountryName(correctAnswer);
        ++totalGuesses; // Увеличиваем стату пользователя по ответам.

        //если корректный
        if (guess.equals(answer)) {
            ++correctAnswers; // стата по правильным ответам.

            //Рисуем зеленый Correct!
            answerTextView.setText(answer + "!");
            //answerTextView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            answerTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

            disableButtons();

            // Если пользватель 10 раз угадал =)
            if (correctAnswers == 10) {
                //AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.reset_quiz);
                builder.setMessage(String.format("%d %s, %.02f%% %s", totalGuesses, getResources().getString(R.string.guesses),
                        (1000 / (double) totalGuesses),
                        getResources().getString(R.string.correct)));
                builder.setCancelable(false);

                builder.setPositiveButton(R.string.reset_quiz, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        resetQuiz();
                    }
                });

                AlertDialog resetDialog = builder.create();
                resetDialog.show();
            }
            else {  // корректный ответ но викторина еще не завершена.
                // отображаем следующий вопрос
                handler.postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                loadNextFlag(false);
                            }
                        }
                , 1000);  // секундный дилэй
            }
        }
        else {  //некорректный вариант
            // Анимация на флаг для неверного ответа
            flagImageView.startAnimation(shakeAnimation);
            // Красный неправильный ответ
            answerTextView.setText(R.string.incorrect_answer);
            answerTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.incorrect_answer));
            guessButton.setEnabled(false); // отключение неверного ответа
        }
    }

    private void disableButtons() {
        for (int row = 0; row < buttonTableLayout.getChildCount(); ++row) {
            TableRow tableRow = (TableRow) buttonTableLayout.getChildAt(row);
            for (int i = 0; i < tableRow.getChildCount(); ++i) {
                tableRow.getChildAt(i).setEnabled(false);
            }
        }
    }
}
